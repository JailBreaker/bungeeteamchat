package eu.jailbreaker.teamchat;

import com.google.common.collect.Maps;
import eu.jailbreaker.teamchat.commands.TeamChatCommand;
import eu.jailbreaker.teamchat.listener.LoginListener;
import eu.jailbreaker.teamchat.universal.TeamChatHandler;
import eu.jailbreaker.teamchat.universal.TeamChatPlayer;
import lombok.Getter;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.Map;
import java.util.UUID;

@Getter
public class TeamChat extends Plugin {

    public static final Map<UUID, TeamChatPlayer> PLAYER_MAP = Maps.newHashMap();

    @Getter
    private static TeamChat instance;

    private TeamChatHandler handler;

    @Override
    public void onEnable() {
        instance = this;

        Messages.load(this);

        this.handler = new TeamChatHandler();

        this.getProxy().getPluginManager().registerListener(this, new LoginListener(this));
        this.getProxy().getPluginManager().registerCommand(this, new TeamChatCommand());
    }

    @Override
    public void onDisable() {
        if (this.handler == null) return;
        this.handler.close();
    }
}
