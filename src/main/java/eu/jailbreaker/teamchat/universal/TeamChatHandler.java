package eu.jailbreaker.teamchat.universal;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class TeamChatHandler {

    private final SessionFactory factory;

    public TeamChatHandler() {
        factory = HibernateConnector.initSessionFactory();
    }

    public void close() {
        if (this.factory == null) return;
        this.factory.close();
    }

    public TeamChatPlayer find(@NotNull UUID uniqueId) {
        try (Session session = this.factory.openSession()) {
            return (TeamChatPlayer) session.createCriteria(TeamChatPlayer.class)
                    .add(Restrictions.eq("uniqueId", uniqueId.toString()))
                    .setMaxResults(1)
                    .uniqueResult();
        }
    }

    public CompletableFuture<TeamChatPlayer> findAsync(@NotNull UUID uniqueId) {
        return CompletableFuture.supplyAsync(() -> this.find(uniqueId));
    }

    public void create(@NotNull TeamChatPlayer teamChatPlayer) {
        try (Session session = this.factory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(teamChatPlayer);
            transaction.commit();
        }
    }

    public void setNotify(@NotNull TeamChatPlayer teamChatPlayer, boolean notify) {
        teamChatPlayer.setNotify(notify);
        this.update(teamChatPlayer);
    }

    private void update(@NotNull TeamChatPlayer teamChatPlayer) {
        CompletableFuture.runAsync(() -> {
            try (Session session = this.factory.openSession()) {
                Transaction transaction = session.beginTransaction();
                session.update(teamChatPlayer);
                transaction.commit();
            }
        });
    }

}
