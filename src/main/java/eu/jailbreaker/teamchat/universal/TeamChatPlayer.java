package eu.jailbreaker.teamchat.universal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class TeamChatPlayer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String uniqueId;

    private boolean notify = true;

    public TeamChatPlayer(@NotNull UUID uniqueId) {
        this.uniqueId = uniqueId.toString();
    }

}
