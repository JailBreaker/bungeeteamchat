package eu.jailbreaker.teamchat.universal;

import eu.jailbreaker.teamchat.TeamChat;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.io.FileReader;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class HibernateConnector {

    @SneakyThrows
    public static SessionFactory initSessionFactory() {
        Configuration configuration = new Configuration();

        Properties properties = null;

        Path path = Paths.get(TeamChat.getInstance().getDataFolder().toString(), "hibernate.properties");

        if (Files.notExists(path)) {
            InputStream in = HibernateConnector.class.getClassLoader().getResourceAsStream("hibernate.properties");

            if (in == null) {
                properties = defaultProperties();
            } else {
                Files.copy(in, path);
            }
        }

        if (properties == null) {
            properties = new Properties();

            @Cleanup
            FileReader reader = new FileReader(path.toFile());

            properties.load(reader);
        }

        configuration.setProperties(properties);

        configuration.addAnnotatedClass(TeamChatPlayer.class);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();

        return configuration.buildSessionFactory(serviceRegistry);
    }

    private static Properties defaultProperties() {
        Properties properties = new Properties();

        properties.setProperty(Environment.DRIVER, "com.mysql.jdbc.Driver");
        properties.setProperty(Environment.URL, "jdbc:mysql://localhost:3306/database");
        properties.setProperty(Environment.USER, "root");
        properties.setProperty(Environment.PASS, "");
        properties.setProperty(Environment.HBM2DDL_AUTO, "update");
        properties.setProperty(Environment.SHOW_SQL, "true");
        properties.setProperty(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
        properties.setProperty(Environment.ORDER_INSERTS, "true");
        properties.setProperty(Environment.ORDER_UPDATES, "true");
        properties.setProperty(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");

        return properties;
    }
}
