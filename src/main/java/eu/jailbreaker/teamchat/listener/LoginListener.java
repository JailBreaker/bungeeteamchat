package eu.jailbreaker.teamchat.listener;

import eu.jailbreaker.teamchat.TeamChat;
import eu.jailbreaker.teamchat.universal.TeamChatPlayer;
import lombok.RequiredArgsConstructor;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.UUID;

@RequiredArgsConstructor
public class LoginListener implements Listener {

    private final TeamChat instance;

    @EventHandler
    public void onLogin(LoginEvent event) {
        event.registerIntent(TeamChat.getInstance());

        // AsyncEvent => synchronous query

        UUID uniqueId = event.getConnection().getUniqueId();
        TeamChatPlayer player = this.instance.getHandler().find(uniqueId);

        if (player == null) {
            player = new TeamChatPlayer(uniqueId);
            this.instance.getHandler().create(player);
        }

        TeamChat.PLAYER_MAP.put(uniqueId, player);

        event.completeIntent(TeamChat.getInstance());
    }

    @EventHandler
    public void onDisconnect(PlayerDisconnectEvent event) {
        TeamChat.PLAYER_MAP.remove(event.getPlayer().getUniqueId());
    }

}
