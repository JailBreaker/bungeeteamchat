package eu.jailbreaker.teamchat.commands;

import com.google.common.base.Joiner;
import eu.jailbreaker.teamchat.Messages;
import eu.jailbreaker.teamchat.TeamChat;
import eu.jailbreaker.teamchat.universal.TeamChatPlayer;
import lombok.SneakyThrows;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class TeamChatCommand extends Command {

    public TeamChatCommand() {
        super("teamchat", "eu.jailbreaker.teamchat", "tc", "teamc");
    }

    @SneakyThrows
    @Override
    public void execute(@NotNull CommandSender sender, @NotNull String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            Messages.send(sender, "must-be-player");
            return;
        }

        if (!sender.hasPermission(this.getPermission())) {
            Messages.send(sender, "no-permission");
            return;
        }

        ProxiedPlayer player = (ProxiedPlayer) sender;

        if (args.length >= 1) {
            if (args[0].equalsIgnoreCase("list")) {
                List<String> proxiedPlayers = TeamChat.PLAYER_MAP.values().stream()
                        .filter(TeamChatPlayer::isNotify)
                        .map(teamChatPlayer -> ProxyServer.getInstance().getPlayer(UUID.fromString(teamChatPlayer.getUniqueId())))
                        .map(ProxiedPlayer::getDisplayName)
                        .sorted(String::compareToIgnoreCase)
                        .collect(Collectors.toList());

                if (proxiedPlayers.isEmpty()) {
                    Messages.send(player, "teamchat-list-empty");
                    return;
                }

                Messages.send(player, "teamchat-list", proxiedPlayers.size(), Joiner.on("§7, ").join(proxiedPlayers));
            } else if (args[0].equalsIgnoreCase("login") || args[0].equalsIgnoreCase("logout")) {
                TeamChatPlayer chatPlayer = TeamChat.PLAYER_MAP.get(player.getUniqueId());

                if (chatPlayer == null) {
                    chatPlayer = TeamChat.getInstance().getHandler().findAsync(player.getUniqueId()).get();
                    TeamChat.PLAYER_MAP.put(player.getUniqueId(), chatPlayer);
                }

                if (args[0].equalsIgnoreCase("login")) {
                    if (chatPlayer.isNotify()) {
                        Messages.send(player, "already-logged-in");
                        return;
                    }

                    TeamChat.getInstance().getHandler().setNotify(chatPlayer, true);
                    Messages.send(player, "command-logged-in");
                } else {
                    if (!chatPlayer.isNotify()) {
                        Messages.send(player, "not-logged-in");
                        return;
                    }

                    TeamChat.getInstance().getHandler().setNotify(chatPlayer, false);
                    Messages.send(player, "command-logged-out");
                }
            } else {
                StringBuilder sb = new StringBuilder();

                for (String arg : args) {
                    if (sb.length() != 0) {
                        sb.append(" ");
                    }
                    sb.append(arg);
                }

                TeamChat.PLAYER_MAP.values().stream()
                        .filter(TeamChatPlayer::isNotify)
                        .map(teamChatPlayer -> ProxyServer.getInstance().getPlayer(UUID.fromString(teamChatPlayer.getUniqueId())))
                        .forEach(chatPlayer -> Messages.send(chatPlayer, "teamchat-broadcast", player.getDisplayName(), sb.toString()));
            }
        } else {
            Messages.send(player, "teamchat-command-help");
        }
    }
}
